package com.cjc.pojo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Test {
public static void main(String[] args) 
{

	Configuration configuration=new Configuration();
	configuration.configure("hibernate.cfg.xml");
	SessionFactory sessionFactory= configuration.buildSessionFactory();
	Session session= sessionFactory.openSession();
	Student student=(Student) session.load(Student.class, new Integer(15));
	System.out.println(student.getAddress().getAddressName());
	System.out.println(student.getStudentName());
    /*Student student=new Student();
    student.setStudentId(15);
    student.setStudentName("adtcool");
    Address address=new Address();
    //address.setAddressId(25);
    address.setAddressName("akurdi");
student.setAddress(address);
address.setStudent(student);
*///session.save(student);
session.beginTransaction().commit();
System.out.println("cool done");
}	

}
